#include <iostream>
#include <vector>
#include <unordered_set>
#include <stack>

using AdjacencyList = std::unordered_set<int>;
using Graph = std::vector<AdjacencyList>;

int CheckEulerPath(const Graph& graph) {
  std::vector<size_t> in_degree(graph.size(), 0);
  std::vector<size_t> out_degree(graph.size(), 0);
  for (int v = 0; v < graph.size(); ++v) {
    out_degree[v] = graph[v].size();
    for (int u : graph[v]) {
      ++in_degree[u];
    }
  }

  int start = -1;
  bool end_found = false;
  for (int v = 0; v < graph.size(); ++v) {
    if (out_degree[v] == in_degree[v] + 1) {
      if (start != -1) {
        return -1;
      }
      start = v;
    } else if (in_degree[v] == out_degree[v] + 1) {
      if (end_found) {
        return -1;
      }
      end_found = true;
    } else if (in_degree[v] != out_degree[v]) {
      return -1;
    }
  }

  if (end_found && start != -1) {
    return start;
  }
  if (!end_found && start == -1) {
    return 0;
  }
  return -1;
}

std::vector<int> FindEulerPath(Graph graph, int start) {
  std::stack<int> s;
  s.push(start);
  std::vector<int> result;
  while (!s.empty()) {
    int v = s.top();
    if (graph[v].empty()) {
      result.push_back(v);
      s.pop();
    } else {
      auto it = graph[v].begin();
      int u = *it;
      graph[v].erase(it);
      s.push(u);
    }
  }
  std::reverse(result.begin(), result.end());
  return result;
}

void Solve() {
  size_t n;
  size_t m;
  std::cin >> n >> m;

  Graph graph(n);
  for (size_t i = 0; i < m; ++i) {
    int u, v;
    std::cin >> u >> v;
    --u;
    --v;
    graph[u].insert(v);
  }

  int start = CheckEulerPath(graph);
  if (start == -1) {
    std::cout << "No euler paths";
  } else {
    auto path = FindEulerPath(graph, start);
    for (int u: path) {
      std::cout << u + 1 << ' ';
    }
  }
}

int main(int argc, char* argv[]) {
  Solve();
}