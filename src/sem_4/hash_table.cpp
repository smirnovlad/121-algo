#include <iostream>
#include <string>
#include <vector>

template<typename Key, typename Value, class Hash = std::hash<Key>, class EqualTo = std::equal_to<Key>>
class HashTable {
 public:
  HashTable() : hash_table_(1) {}
  void Insert(const Key& key, const Value& value);
  void Erase(const Key& key);
  Value& operator[](const Key& key);
  const Value& operator[](const Key& key) const;
  bool Find(const Key& key) const;
  size_t size() const;
 private:
  void Rehash();

  struct Node {
    Key key;
    Value value;
    size_t hash;
  };

  using Bucket = std::vector<Node>;

  std::vector<Bucket> hash_table_;
  size_t size_ = 0;
  Hash hash_;
  EqualTo equal_to_;
  double load_factor_ = 0.95;
};

template<typename Key, typename Value, class Hash, class EqualTo>
void HashTable<Key, Value, Hash, EqualTo>::Insert(const Key& key, const Value& value) {
  if (size_ >= load_factor_ * hash_table_.size()) {
    Rehash();
  }

  size_t hash = hash_(key);
  size_t hash_value = hash % hash_table_.size();
  for (const auto& node : hash_table_[hash_value]) {
    if (equal_to_(key, node.key)) {
      return;
    }
  }

  hash_table_[hash_value].push_back({key, value, hash});
  ++size_;
}

template<typename Key, typename Value, class Hash, class EqualTo>
void HashTable<Key, Value, Hash, EqualTo>::Erase(const Key& key) {
  size_t hash_value = hash_(key) % hash_table_.size();
  for (auto it = hash_table_[hash_value].begin();
       it != hash_table_[hash_value].end();
       ++it) {
    if (equal_to_(key, it->key)) {
      hash_table_[hash_value].erase(it);
      --size_;
      return;
    }
  }
}

template<typename Key, typename Value, class Hash, class EqualTo>
Value& HashTable<Key, Value, Hash, EqualTo>::operator[](const Key& key) {
  return const_cast<Value&>(const_cast<const HashTable*>(this)->operator[](key));
}

template<typename Key, typename Value, class Hash, class EqualTo>
const Value& HashTable<Key, Value, Hash, EqualTo>::operator[](const Key& key) const {
  size_t hash_value = hash_(key) % hash_table_.size();
  for (auto& node : hash_table_[hash_value]) {
    if (equal_to_(key, node.key)) {
      return node.value;
    }
  }
}

template<typename Key, typename Value, class Hash, class EqualTo>
bool HashTable<Key, Value, Hash, EqualTo>::Find(const Key& key) const {
  size_t hash_value = hash_(key) % hash_table_.size();
  for (auto& node : hash_table_[hash_value]) {
    if (equal_to_(key, node.key)) {
      return true;
    }
  }
  return false;
}

template<typename Key, typename Value, class Hash, class EqualTo>
size_t HashTable<Key, Value, Hash, EqualTo>::size() const {
  return size_;
}

template<typename Key, typename Value, class Hash, class EqualTo>
void HashTable<Key, Value, Hash, EqualTo>::Rehash() {
  std::vector<Bucket> new_hash_table(2 * hash_table_.size());
  for (const auto& bucket : hash_table_) {
    for (const auto& node : bucket) {
      new_hash_table[node.hash % new_hash_table.size()].push_back(node);
    }
  }
  hash_table_ = new_hash_table;
}

int main(int argc, char* argv[]) {
  HashTable<std::string, std::string> ht;
  HashTable<int, int>&& x = HashTable<int, int>();
  ht.Insert("asd", "zxc");
  ht.Insert("a", "1");
  ht.Insert("b", "2");
  ht.Insert("c", "3");
  ht.Insert("d", "4");
  ht.Erase("d");
  ht.Erase("asd");

  ht["a"] = "qwe";
  std::cout << ht["a"] << '\n';
}
